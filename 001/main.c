#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define MAX_TEXT 35
#define MAX_COS 10000
#define BB while(getchar()!='\n')

struct Jugador{
    char Nom[MAX_TEXT];
    int Edat;
    char Identificador[MAX_TEXT];
    bool Federat;
    int Puntuacio;
    char Lliga[MAX_TEXT];

};

void entrarDadesJugador(struct Jugador *j);
void sortirDadesJugador(struct Jugador j);
int escriurerJugadorDisc(FILE *fitxer, struct Jugador *j, int registres);
int llegirJugadorDisc(FILE *fitxer, struct Jugador *j);
void obrirHtml(FILE *f,int error, char cap[]);
void crearHtml1(char cos[MAX_COS], FILE *f, char peu[], FILE *fitxer, int error);
void crearHtml(FILE *fitxer, FILE *f, struct Jugador *j, int *error, char cap[], char cos[MAX_COS], char peu[]);



int main()
{
    struct Jugador j;
    FILE *fitxer;
    int numeroPersones,a;
    int error;

    //  ARA FEM LO DEL HTML
    FILE *f;

    char cos[MAX_COS]="";
        char cap[]="<!DOCTYPE html>\n\
                <html lang=\"en\">\n\
                <head>\n\
                    <meta charset=\"UTF-8\">\n\
                    <title>Document</title>\n\
                </head>\n\
                <body>\
                <table border=1><tr><th>Nom</th><th>Edat</th><th>Identificador</th><th>Federat</th><th>Puntuacio</th><th>Lliga</th></tr>";
        char peu[]="\n</table></body>\n</html>";


    printf("Quantes persones vols d'onar de alta?\t");
    scanf("%i",&numeroPersones);BB;

    fitxer=fopen("Jugadors.bin","ab");

    for(int i=0;i<numeroPersones;i++){
        a=escriurerJugadorDisc(fitxer, &j, 1);
    }

    if(fclose(fitxer)){
        printf("\nError tancar fitxer.");
    }


    //LLISTAT A PANTALLA

    fitxer=fopen("Jugadors.bin","rb");
    while(!feof(fitxer)){
        a=llegirJugadorDisc(fitxer, &j);
        if(a==0){
            sortirDadesJugador(j);

        }
        if(a==1){
            printf("Error obrir fitxer"); break;
        }
        if(a==3){
            printf("Error de lectura");break;
        }
    }
     if(fclose(fitxer)){
        printf("\nERROR al tancar el fitxer.");
    }

    fitxer=fopen("Jugadors.bin","rb");

    crearHtml(fitxer, f, &j, &error, cap, cos, peu);


    return 0;
}

void entrarDadesJugador(struct Jugador *j){
    int i=0;
    char a;
    printf("Nom:\t");               scanf("%35[^\n]",j->Nom);BB;
    printf("\nEdat:\t");            scanf("%i",&j->Edat);BB;
    printf("\nIdentificador:\t");   scanf("%35[^\n]",j->Identificador);BB;
    printf("\nFederat[S/N]:\t");         scanf("%c",&a);BB;
    if(a=='S') j->Federat=true;
    else j->Federat=false;
    if(j->Federat==true){
        printf("\nPuntuacio:\t");   scanf("%d",&j->Puntuacio);BB;
        printf("\nLliga:\t");       scanf("%35[^\n]",j->Lliga);BB;
    }
    else{
        j->Puntuacio=0;
        while(i<MAX_TEXT){
            j->Lliga[i]='\0';
            i++;
        }
    }

    printf("\n\n\n");
}

void sortirDadesJugador(struct Jugador j){
    printf("Nom:\t%s",j.Nom);
    printf("\nEdat:\t%d",j.Edat);
    printf("\nIdentificador:\t%s",j.Identificador);
    if(j.Federat==true) printf("\nFederat:\tSi.");
    else printf("\nFederat:\tNo.");
    printf("\nPuntuacio:\t%i",j.Puntuacio);
    printf("\nLliga:\t%s",j.Lliga);
    printf("\n\n\n");
}

int escriurerJugadorDisc(FILE *fitxer, struct Jugador *j, int registres){
    int a, error=0;
    if(fitxer==NULL){
        error=1;    //Error al obrir
    }else{
        entrarDadesJugador(j);
        a=fwrite(j,sizeof(struct Jugador),registres,fitxer);
        if(a!=registres){
            error=2;    //No ha escrit les dades del jugador
        }
    }
    return error;
}



int llegirJugadorDisc(FILE *fitxer, struct Jugador *j){
    int error=0,n=0;
    if(fitxer==NULL)  error=1;
    else{
        n=fread(j,sizeof(struct Jugador),1,fitxer);
        if(fitxer==NULL){
            error=1;
        }
    }
    return error;
}

void obrirHtml(FILE *f,int error, char cap[]){

    f=fopen("index.html","w");
     error=fputs(cap,f);
            if(error==EOF){
                error=2;
            }

}

void crearHtml(FILE *fitxer, FILE *f, struct Jugador *j, int *error, char cap[], char cos[MAX_COS], char peu[]){
    int a;

    char cadenaTmp[100];
    char si[3]="Si";
    char no[3]="No";

    obrirHtml(f, *error, cap);


    while(!feof(fitxer)){
        a=llegirJugadorDisc(fitxer, j);
        if(a==0){
            strcat(cos,"<tr><td>");
            strcat(cos,j->Nom);
            strcat(cos,"</td><td>");
            sprintf(cadenaTmp,"%d", j->Edat);
            strcat(cos, cadenaTmp);
            strcat(cos,"</td><td>");
            strcat(cos,j->Identificador);
            strcat(cos,"</td><td>");
            if(j->Federat==true) strcat(cos,si);
            else strcat(cos,no);
            strcat(cos,"</td><td>");
            sprintf(cadenaTmp,"%d",j->Puntuacio);
            strcat(cos, cadenaTmp);
            strcat(cos,"</td><td>");
            strcat(cos,j->Lliga);
            strcat(cos,"</td></tr>");

        }
        if(a==1){
            printf("Error obrir fitxer"); break;
        }
        if(a==3){
            printf("Error de lectura");break;
        }
    }
    crearHtml1(cos, f, peu, fitxer, *error);
}

void crearHtml1(char cos[MAX_COS], FILE *f, char peu[], FILE *fitxer, int error){

    error=fputs(cos,f);
            if(error==EOF){
                error=2;
            }
    error=fputs(peu,f);
            if(error==EOF){
                error=2;
            }
    if(fclose(fitxer)){
        printf("\nERROR al tancar el fitxer.");
    }
    if(fclose(f)){
        printf("Error de lectura");
    }
}
